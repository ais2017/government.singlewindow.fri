#[warn(unused_variables)]

use std::vec::*;
use std::clone::Clone;
use std::collections::HashMap;

#[derive(PartialEq, Debug)]
pub struct DocumentType {
    name: String,
}

impl DocumentType {
    pub fn new(name: String) -> DocumentType {
        DocumentType { name: name }
    }

    pub fn set_name(&mut self, name: String) {
        self.name = name;
    }

    pub fn name(&self) -> &String {
        &self.name
    }
}

impl Clone for DocumentType {
    fn clone(&self) -> DocumentType {
        DocumentType { name: self.name.clone() }
    }
}

#[derive(PartialEq, Debug)]
pub struct ServiceType {
    name: String,
    documents: Vec<DocumentType>
}

impl ServiceType {
    pub fn new(name: String,
               documents: Vec<DocumentType>) -> Option<ServiceType> {
        if documents.len() < 1 {
            None
        } else {
            Some(ServiceType { name: name, documents: documents })
        }
    }

    pub fn set_name(&mut self, name: String) {
        self.name = name;
    }

    pub fn name(&self) -> &String {
        &self.name
    }

    pub fn add_document(&mut self, doc: DocumentType) {
        self.documents.push(doc);
    }

    pub fn documents(&self) -> &Vec<DocumentType> {
        &self.documents
    }

    pub fn get_document(&self, name: String) -> Option<&DocumentType> {
        for i in self.documents() {
            if i.name == name {
                return Some(&i)
            }
        }
        None
    }
}

impl Clone for ServiceType {
    fn clone(&self) -> ServiceType {
        ServiceType { name: self.name.clone(),
                      documents: self.documents.clone() }
    }
}

#[derive(PartialEq, Debug)]
pub struct Service {
    id: u32,
    service_type: ServiceType,
    result: String,
    citizen_informed: bool
}

impl Service {
    pub fn get_free_id() -> u32 {
        0
    }

    pub fn new(id: u32, service_type: ServiceType) -> Service {
        Service { id: id, service_type: service_type, result: "".to_string(),
                  citizen_informed: false}
    }

    pub fn id(&self) -> u32 {
        self.id
    }

    pub fn set_id(&mut self, id: u32) {
        self.id = id;
    }

    pub fn service_type(&self) -> &ServiceType {
        &self.service_type
    }

    pub fn set_service_type(&mut self, service_type: ServiceType) {
        self.service_type = service_type;
    }

    pub fn result(&self) -> &String {
        &self.result
    }

    pub fn set_result(&mut self, result: String) {
        self.result = result;
    }

    pub fn citizen_informed(&self) -> &bool {
        &self.citizen_informed
    }

    pub fn set_citizen_informed(&mut self, val: bool) {
        self.citizen_informed = val;
    }
}

impl Clone for Service {
    fn clone (&self) -> Service {
        Service { id: self.id, service_type: self.service_type.clone(),
                  result: self.result.clone(),
                  citizen_informed: self.citizen_informed }
    }
}

#[derive(PartialEq, Debug)]
pub struct Message {
    subject: String,
    body: String,
    destination: String
}

impl Message {
    pub fn new(subject: String, body: String, destination: String) -> Message {
        Message { subject: subject, body: body, destination: destination }
    }

    pub fn subject(&self) -> &String {
        &self.subject
    }

    pub fn set_subject(&mut self, subject: String) {
        self.subject = subject;
    }

    pub fn body(&self) -> &String {
        &self.body
    }

    pub fn set_body(&mut self, body: String) {
        self.body = body;
    }

    pub fn destination(&self) -> &String {
        &self.destination
    }

    pub fn set_destination(&mut self, destination: String) {
        self.destination = destination;
    }

    pub fn send(&self) {

    }
}

#[derive(PartialEq, Debug)]
pub struct Citizen {
    name: String,
    passport_id: String,
    services: Vec<Service>,
    additional_data: HashMap<String, String>,
    email: String
}

static mut register_success: bool = true;
static mut get_citizen_success: bool = true;
static mut analyze_and_fill_data_success: bool = true;

impl Citizen {
    pub fn new(name: String, passport_id: String,
               additional_data: HashMap<String, String>,
               email: String) -> Citizen {
        Citizen { name: name, passport_id: passport_id,
                  additional_data: additional_data.clone(),
                  email: email, services: Vec::new() }
    }

    pub fn name(&self) -> &String {
        &self.name
    }

    pub fn set_name(&mut self, name: String) {
        self.name = name;
    }

    pub fn passport_id(&self) -> &String {
        &self.passport_id
    }

    pub fn set_passport_id(&mut self, passport_id: String) {
        self.passport_id = passport_id;
    }

    pub fn email(&self) -> &String {
        &self.email
    }

    pub fn set_email(&mut self, email: String) {
        self.email = email;
    }

    pub fn add_service(&mut self, service: Service) {
        self.services.push(service);
    }

    pub fn services(&self) -> &Vec<Service> {
        &self.services
    }

    pub fn get_service(&self, id: u32) -> Option<&Service> {
        for i in self.services() {
            if i.id == id {
                return Some(&i)
            }
        }
        None
    }

    pub fn delete_service(&mut self, id: u32) {
        for n in 0..self.services.len() {
            if self.services[n].id() == id {
                self.services.remove(n);
                break;
            }
        }
    }

    pub fn mark_service_as_completed(&mut self, id: u32) {
        let mut service_index: usize = 0;
        for n in 0..self.services.len() {
            if self.services[n].id() == id {
                service_index = n;
                break;
            }
        }
        if service_index == 0 {
            return
        }
        self.services[service_index].result = "complete".to_string();
        self.services[service_index].citizen_informed = true;
        Message::new("Service provide result (complete)".to_string(),
                     "...".to_string(),
                     self.email.clone()).send();
    }

    pub fn get_additional_data(&self, key: String) -> Option<&String> {
        self.additional_data.get(&key)
    }

    pub fn set_additional_data(&mut self, key: String, value: String) {
        self.additional_data.insert(key, value);
    }

    pub fn get_citizen_from_db(passport_id: String) -> Option<Citizen> {
        unsafe {
            if !get_citizen_success {
                return None
            }
        }
        let additional_data: HashMap<String, String> =
            [("Sex".to_string(), "Male".to_string()),
             ("Age".to_string(), "50".to_string()),
             ("Country".to_string(), "Russia".to_string())]
            .iter().cloned().collect();
        Some(Citizen::new("Uncle Bafomet".to_string(),
                          "4616051117".to_string(),
                          additional_data,
                          "dyadya_bafomet@sosach.hk".to_string()))
    }

    pub fn register_citizen() -> Option<Citizen> {
        unsafe {
            if !register_success {
                println!("register fails");
                return None
            }
        }
        let additional_data: HashMap<String, String> =
            [("Sex".to_string(), "Male".to_string()),
             ("Age".to_string(), "50".to_string()),
             ("Country".to_string(), "Russia".to_string())]
            .iter().cloned().collect();
        Some(Citizen::new("Uncle Bafomet".to_string(),
                          "4616051117".to_string(),
                          additional_data,
                          "dyadya_bafomet@sosach.hk".to_string()))
    }

    pub fn analyze_and_fill_data(&mut self) -> bool {
        unsafe {
            analyze_and_fill_data_success
        }
    }
}

pub fn provide_service(passport_id: String, service_type: ServiceType) -> u32 {
    let mut citizen = match Citizen::get_citizen_from_db(passport_id) {
        Some(x) => x,
        None => match Citizen::register_citizen() {
            Some(y) => y,
            None => return 1
        }
    };
    if !citizen.analyze_and_fill_data() {
        return 3
    }
    let id = Service::get_free_id();
    citizen.add_service(Service::new(id, service_type));
    match citizen.get_service(id) {
        Some(_) => 0,
        None => 2
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_service_type_new() {
        let st = ServiceType::new(
            "Register citizen".to_string(),
            vec![DocumentType::new("Passport".to_string()); 1]
        );
        assert!(st.is_some())
    }

    #[test]
    fn test_service_type_new_fail() {
        assert_eq!(ServiceType::new("Register citizen".to_string(), Vec::new()),
                   None)
    }

    #[test]
    fn test_service_type_add_document() {
        let mut st = ServiceType::new(
            "Register citizen".to_string(),
            vec![DocumentType::new("Passport".to_string()); 1]
        ).unwrap();
        st.add_document(DocumentType::new("Driving license".to_string()));
        assert_eq!(st.documents.len(), 2);
    }

    #[test]
    fn test_service_type_get_document() {
        let st = ServiceType::new(
            "Register citizen".to_string(),
            vec![DocumentType::new("Passport".to_string()),
                 DocumentType::new("Driving license".to_string())]
        ).unwrap();
        assert_eq!(*st.get_document("Passport".to_string()).unwrap(),
                   DocumentType::new("Passport".to_string()))
    }

    #[test]
    fn test_service_type_get_document_fail() {
        let st = ServiceType::new(
            "Register citizen".to_string(),
            vec![DocumentType::new("Passport".to_string()),
                 DocumentType::new("Driving license".to_string())]
        ).unwrap();
        assert_eq!(st.get_document("No such document".to_string()),
                   None)
    }

    #[test]
    fn test_citizen_add_service() {
        let documents = vec![DocumentType::new("Passport".to_string()),
                             DocumentType::new("Driving license".to_string())];
        let st = ServiceType::new("Register citizen".to_string(), documents)
            .unwrap();
        let additional_data: HashMap<String, String> =
            [("Sex".to_string(), "Male".to_string()),
             ("Age".to_string(), "50".to_string()),
             ("Country".to_string(), "Russia".to_string())]
            .iter().cloned().collect();
        let mut c = Citizen::new("Uncle Bafomet".to_string(),
                                 "4616051117".to_string(),
                                 additional_data,
                                 "dyadya_bafomet@sosach.hk".to_string());
        c.add_service(Service::new(1, st.clone()));
        assert_eq!(c.services.len(), 1);
        c.add_service(Service::new(2, st.clone()));
        assert_eq!(c.services.len(), 2);
    }

    #[test]
    fn test_citizen_get_service() {
        let documents = vec![DocumentType::new("Passport".to_string()),
                             DocumentType::new("Driving license".to_string())];
        let st = ServiceType::new("Register citizen".to_string(), documents)
            .unwrap();
        let additional_data: HashMap<String, String> =
            [("Sex".to_string(), "Male".to_string()),
             ("Age".to_string(), "50".to_string()),
             ("Country".to_string(), "Russia".to_string())]
            .iter().cloned().collect();
        let mut c = Citizen::new("Uncle Bafomet".to_string(),
                                 "4616051117".to_string(),
                                 additional_data,
                                 "dyadya_bafomet@sosach.hk".to_string());
        c.add_service(Service::new(1, st.clone()));
        c.add_service(Service::new(2, st.clone()));
        assert_eq!(*c.get_service(1).unwrap(), Service::new(1, st.clone()));
    }

    #[test]
    fn test_citizen_get_service_fail() {
        let documents = vec![DocumentType::new("Passport".to_string()),
                             DocumentType::new("Driving license".to_string())];
        let st = ServiceType::new("Register citizen".to_string(), documents)
            .unwrap();
        let additional_data: HashMap<String, String> =
            [("Sex".to_string(), "Male".to_string()),
             ("Age".to_string(), "50".to_string()),
             ("Country".to_string(), "Russia".to_string())]
            .iter().cloned().collect();
        let mut c = Citizen::new("Uncle Bafomet".to_string(),
                                 "4616051117".to_string(),
                                 additional_data,
                                 "dyadya_bafomet@sosach.hk".to_string());
        c.add_service(Service::new(1, st.clone()));
        c.add_service(Service::new(2, st.clone()));
        assert_eq!(c.get_service(3), None);
    }

    #[test]
    fn test_citizen_delete_service() {
        let documents = vec![DocumentType::new("Passport".to_string()),
                             DocumentType::new("Driving license".to_string())];
        let st = ServiceType::new("Register citizen".to_string(), documents)
            .unwrap();
        let additional_data: HashMap<String, String> =
            [("Sex".to_string(), "Male".to_string()),
             ("Age".to_string(), "50".to_string()),
             ("Country".to_string(), "Russia".to_string())]
            .iter().cloned().collect();
        let mut c = Citizen::new("Uncle Bafomet".to_string(),
                                 "4616051117".to_string(),
                                 additional_data,
                                 "dyadya_bafomet@sosach.hk".to_string());
        c.add_service(Service::new(1, st.clone()));
        c.add_service(Service::new(2, st.clone()));
        c.delete_service(1);
        assert_eq!(c.services()[0].id(), 2)
    }

    #[test]
    fn test_citizen_mark_service_as_completed() {
        let documents = vec![DocumentType::new("Passport".to_string()),
                             DocumentType::new("Driving license".to_string())];
        let st = ServiceType::new("Register citizen".to_string(), documents)
            .unwrap();
        let additional_data: HashMap<String, String> =
            [("Sex".to_string(), "Male".to_string()),
             ("Age".to_string(), "50".to_string()),
             ("Country".to_string(), "Russia".to_string())]
            .iter().cloned().collect();
        let mut c = Citizen::new("Uncle Bafomet".to_string(),
                                 "4616051117".to_string(),
                                 additional_data,
                                 "dyadya_bafomet@sosach.hk".to_string());
        c.add_service(Service::new(1, st.clone()));
        c.add_service(Service::new(2, st.clone()));
        c.mark_service_as_completed(2);
        let s = c.get_service(2).unwrap();
        assert_eq!(s.citizen_informed, true);
        assert_eq!(s.result, "complete");
    }

    #[test]
    fn test_citizen_get_additional_data() {
        let documents = vec![DocumentType::new("Passport".to_string()),
                             DocumentType::new("Driving license".to_string())];
        let st = ServiceType::new("Register citizen".to_string(), documents)
            .unwrap();
        let additional_data: HashMap<String, String> =
            [("Sex".to_string(), "Male".to_string()),
             ("Age".to_string(), "50".to_string()),
             ("Country".to_string(), "Russia".to_string())]
            .iter().cloned().collect();
        let mut c = Citizen::new("Uncle Bafomet".to_string(),
                                 "4616051117".to_string(),
                                 additional_data,
                                 "dyadya_bafomet@sosach.hk".to_string());
        c.add_service(Service::new(1, st.clone()));
        c.add_service(Service::new(2, st.clone()));
        assert_eq!(*c.get_additional_data("Sex".to_string()).unwrap(),
                   "Male".to_string());
    }

    #[test]
    fn test_citizen_get_additional_data_fail() {
        let documents = vec![DocumentType::new("Passport".to_string()),
                             DocumentType::new("Driving license".to_string())];
        let st = ServiceType::new("Register citizen".to_string(), documents)
            .unwrap();
        let additional_data: HashMap<String, String> =
            [("Sex".to_string(), "Male".to_string()),
             ("Age".to_string(), "50".to_string()),
             ("Country".to_string(), "Russia".to_string())]
            .iter().cloned().collect();
        let mut c = Citizen::new("Uncle Bafomet".to_string(),
                                 "4616051117".to_string(),
                                 additional_data,
                                 "dyadya_bafomet@sosach.hk".to_string());
        c.add_service(Service::new(1, st.clone()));
        c.add_service(Service::new(2, st.clone()));
        assert_eq!(c.get_additional_data("non-exst".to_string()),
                   None);
    }

    #[test]
    fn test_citizen_set_additional_data() {
        let documents = vec![DocumentType::new("Passport".to_string()),
                             DocumentType::new("Driving license".to_string())];
        let st = ServiceType::new("Register citizen".to_string(), documents)
            .unwrap();
        let additional_data: HashMap<String, String> =
            [("Sex".to_string(), "Male".to_string()),
             ("Age".to_string(), "50".to_string()),
             ("Country".to_string(), "Russia".to_string())]
            .iter().cloned().collect();
        let mut c = Citizen::new("Uncle Bafomet".to_string(),
                                 "4616051117".to_string(),
                                 additional_data,
                                 "dyadya_bafomet@sosach.hk".to_string());
        c.add_service(Service::new(1, st.clone()));
        c.add_service(Service::new(2, st.clone()));
        c.set_additional_data("Country".to_string(), "USA".to_string());
        assert_eq!(*c.additional_data[&"Country".to_string()],
                   "USA".to_string());
    }

    #[test]
    fn test_successful_provide_service() {
        unsafe {
            register_success = true;
            analyze_and_fill_data_success = true;
            get_citizen_success = true;
        };
        let documents = vec![DocumentType::new("Passport".to_string()),
                             DocumentType::new("Driving license".to_string())];
        let st = ServiceType::new("Register citizen".to_string(), documents)
            .unwrap();
        assert_eq!(provide_service("0".to_string(), st), 0)
    }

    #[test]
    fn test_get_fails_provide_service() {
        unsafe {
            register_success = true;
            analyze_and_fill_data_success = true;
            get_citizen_success = false;
        };
        let documents = vec![DocumentType::new("Passport".to_string()),
                             DocumentType::new("Driving license".to_string())];
        let st = ServiceType::new("Register citizen".to_string(), documents)
            .unwrap();
        assert_eq!(provide_service("0".to_string(), st), 0)
    }

    #[test]
    fn test_register_fails_provide_service() {
        unsafe {
            register_success = false;
            analyze_and_fill_data_success = true;
            get_citizen_success = false;
        };
        let documents = vec![DocumentType::new("Passport".to_string()),
                             DocumentType::new("Driving license".to_string())];
        let st = ServiceType::new("Register citizen".to_string(), documents)
            .unwrap();
        assert_eq!(provide_service("0".to_string(), st), 1)
    }

    #[test]
    fn test_analyze_fails_provide_service() {
        unsafe {
            register_success = true;
            analyze_and_fill_data_success = false;
            get_citizen_success = true;
        };
        let documents = vec![DocumentType::new("Passport".to_string()),
                             DocumentType::new("Driving license".to_string())];
        let st = ServiceType::new("Register citizen".to_string(), documents)
            .unwrap();
        assert_eq!(provide_service("0".to_string(), st), 3)
    }
}
