#![feature(vec_remove_item)]
#![feature(plugin)]
#![plugin(rocket_codegen)]

#![allow(unused_variables)]
#![allow(dead_code)]

extern crate rocket;

mod domain;

use domain::DocumentType;

#[get("/")]
fn index() -> &'static str {
    "Hello, world!"
}

fn main() {
    let a = DocumentType::new("qwe".to_string());
    rocket::ignite().mount("/", routes![index]).launch();
}
